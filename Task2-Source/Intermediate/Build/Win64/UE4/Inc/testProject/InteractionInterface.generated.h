// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECT_InteractionInterface_generated_h
#error "InteractionInterface.generated.h already included, missing '#pragma once' in InteractionInterface.h"
#endif
#define TESTPROJECT_InteractionInterface_generated_h

#define Task2_Source_Source_testProject_InteractionInterface_h_12_SPARSE_DATA
#define Task2_Source_Source_testProject_InteractionInterface_h_12_RPC_WRAPPERS \
	virtual void InteractionEvent_Implementation() {}; \
 \
	DECLARE_FUNCTION(execInteractionEvent);


#define Task2_Source_Source_testProject_InteractionInterface_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void InteractionEvent_Implementation() {}; \
 \
	DECLARE_FUNCTION(execInteractionEvent);


#define Task2_Source_Source_testProject_InteractionInterface_h_12_EVENT_PARMS
#define Task2_Source_Source_testProject_InteractionInterface_h_12_CALLBACK_WRAPPERS
#define Task2_Source_Source_testProject_InteractionInterface_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TESTPROJECT_API UInteractionInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractionInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TESTPROJECT_API, UInteractionInterface); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractionInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TESTPROJECT_API UInteractionInterface(UInteractionInterface&&); \
	TESTPROJECT_API UInteractionInterface(const UInteractionInterface&); \
public:


#define Task2_Source_Source_testProject_InteractionInterface_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TESTPROJECT_API UInteractionInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TESTPROJECT_API UInteractionInterface(UInteractionInterface&&); \
	TESTPROJECT_API UInteractionInterface(const UInteractionInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TESTPROJECT_API, UInteractionInterface); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractionInterface); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractionInterface)


#define Task2_Source_Source_testProject_InteractionInterface_h_12_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteractionInterface(); \
	friend struct Z_Construct_UClass_UInteractionInterface_Statics; \
public: \
	DECLARE_CLASS(UInteractionInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/testProject"), TESTPROJECT_API) \
	DECLARE_SERIALIZER(UInteractionInterface)


#define Task2_Source_Source_testProject_InteractionInterface_h_12_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Task2_Source_Source_testProject_InteractionInterface_h_12_GENERATED_UINTERFACE_BODY() \
	Task2_Source_Source_testProject_InteractionInterface_h_12_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Task2_Source_Source_testProject_InteractionInterface_h_12_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Task2_Source_Source_testProject_InteractionInterface_h_12_GENERATED_UINTERFACE_BODY() \
	Task2_Source_Source_testProject_InteractionInterface_h_12_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Task2_Source_Source_testProject_InteractionInterface_h_12_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteractionInterface() {} \
public: \
	typedef UInteractionInterface UClassType; \
	typedef IInteractionInterface ThisClass; \
	static void Execute_InteractionEvent(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Task2_Source_Source_testProject_InteractionInterface_h_12_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteractionInterface() {} \
public: \
	typedef UInteractionInterface UClassType; \
	typedef IInteractionInterface ThisClass; \
	static void Execute_InteractionEvent(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Task2_Source_Source_testProject_InteractionInterface_h_9_PROLOG \
	Task2_Source_Source_testProject_InteractionInterface_h_12_EVENT_PARMS


#define Task2_Source_Source_testProject_InteractionInterface_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Task2_Source_Source_testProject_InteractionInterface_h_12_SPARSE_DATA \
	Task2_Source_Source_testProject_InteractionInterface_h_12_RPC_WRAPPERS \
	Task2_Source_Source_testProject_InteractionInterface_h_12_CALLBACK_WRAPPERS \
	Task2_Source_Source_testProject_InteractionInterface_h_12_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Task2_Source_Source_testProject_InteractionInterface_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Task2_Source_Source_testProject_InteractionInterface_h_12_SPARSE_DATA \
	Task2_Source_Source_testProject_InteractionInterface_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Task2_Source_Source_testProject_InteractionInterface_h_12_CALLBACK_WRAPPERS \
	Task2_Source_Source_testProject_InteractionInterface_h_12_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECT_API UClass* StaticClass<class UInteractionInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Task2_Source_Source_testProject_InteractionInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
