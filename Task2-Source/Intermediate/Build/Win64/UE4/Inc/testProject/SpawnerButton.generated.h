// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECT_SpawnerButton_generated_h
#error "SpawnerButton.generated.h already included, missing '#pragma once' in SpawnerButton.h"
#endif
#define TESTPROJECT_SpawnerButton_generated_h

#define Task2_Source_Source_testProject_SpawnerButton_h_16_SPARSE_DATA
#define Task2_Source_Source_testProject_SpawnerButton_h_16_RPC_WRAPPERS
#define Task2_Source_Source_testProject_SpawnerButton_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Task2_Source_Source_testProject_SpawnerButton_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnerButton(); \
	friend struct Z_Construct_UClass_ASpawnerButton_Statics; \
public: \
	DECLARE_CLASS(ASpawnerButton, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/testProject"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerButton) \
	virtual UObject* _getUObject() const override { return const_cast<ASpawnerButton*>(this); }


#define Task2_Source_Source_testProject_SpawnerButton_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASpawnerButton(); \
	friend struct Z_Construct_UClass_ASpawnerButton_Statics; \
public: \
	DECLARE_CLASS(ASpawnerButton, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/testProject"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerButton) \
	virtual UObject* _getUObject() const override { return const_cast<ASpawnerButton*>(this); }


#define Task2_Source_Source_testProject_SpawnerButton_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnerButton(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnerButton) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerButton); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerButton); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerButton(ASpawnerButton&&); \
	NO_API ASpawnerButton(const ASpawnerButton&); \
public:


#define Task2_Source_Source_testProject_SpawnerButton_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerButton(ASpawnerButton&&); \
	NO_API ASpawnerButton(const ASpawnerButton&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerButton); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerButton); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnerButton)


#define Task2_Source_Source_testProject_SpawnerButton_h_16_PRIVATE_PROPERTY_OFFSET
#define Task2_Source_Source_testProject_SpawnerButton_h_13_PROLOG
#define Task2_Source_Source_testProject_SpawnerButton_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Task2_Source_Source_testProject_SpawnerButton_h_16_PRIVATE_PROPERTY_OFFSET \
	Task2_Source_Source_testProject_SpawnerButton_h_16_SPARSE_DATA \
	Task2_Source_Source_testProject_SpawnerButton_h_16_RPC_WRAPPERS \
	Task2_Source_Source_testProject_SpawnerButton_h_16_INCLASS \
	Task2_Source_Source_testProject_SpawnerButton_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Task2_Source_Source_testProject_SpawnerButton_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Task2_Source_Source_testProject_SpawnerButton_h_16_PRIVATE_PROPERTY_OFFSET \
	Task2_Source_Source_testProject_SpawnerButton_h_16_SPARSE_DATA \
	Task2_Source_Source_testProject_SpawnerButton_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Task2_Source_Source_testProject_SpawnerButton_h_16_INCLASS_NO_PURE_DECLS \
	Task2_Source_Source_testProject_SpawnerButton_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECT_API UClass* StaticClass<class ASpawnerButton>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Task2_Source_Source_testProject_SpawnerButton_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
