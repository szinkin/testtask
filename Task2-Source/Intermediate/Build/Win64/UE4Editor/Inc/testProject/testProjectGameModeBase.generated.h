// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTPROJECT_testProjectGameModeBase_generated_h
#error "testProjectGameModeBase.generated.h already included, missing '#pragma once' in testProjectGameModeBase.h"
#endif
#define TESTPROJECT_testProjectGameModeBase_generated_h

#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_SPARSE_DATA
#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_RPC_WRAPPERS
#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAtestProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AtestProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AtestProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/testProject"), NO_API) \
	DECLARE_SERIALIZER(AtestProjectGameModeBase)


#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAtestProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AtestProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AtestProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/testProject"), NO_API) \
	DECLARE_SERIALIZER(AtestProjectGameModeBase)


#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AtestProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AtestProjectGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AtestProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AtestProjectGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AtestProjectGameModeBase(AtestProjectGameModeBase&&); \
	NO_API AtestProjectGameModeBase(const AtestProjectGameModeBase&); \
public:


#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AtestProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AtestProjectGameModeBase(AtestProjectGameModeBase&&); \
	NO_API AtestProjectGameModeBase(const AtestProjectGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AtestProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AtestProjectGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AtestProjectGameModeBase)


#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Task2_Source_Source_testProject_testProjectGameModeBase_h_12_PROLOG
#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_SPARSE_DATA \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_RPC_WRAPPERS \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_INCLASS \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Task2_Source_Source_testProject_testProjectGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_SPARSE_DATA \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Task2_Source_Source_testProject_testProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTPROJECT_API UClass* StaticClass<class AtestProjectGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Task2_Source_Source_testProject_testProjectGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
