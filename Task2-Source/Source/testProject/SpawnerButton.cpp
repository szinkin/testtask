// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnerButton.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

// Sets default values
ASpawnerButton::ASpawnerButton()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = Mesh;
	Mesh->SetStaticMesh(LoadObject<UStaticMesh>(NULL, TEXT("StaticMesh'/Game/StaticMeshes/SM_Button.SM_Button'"), NULL, LOAD_None, NULL));
}
//������ ������ ���� ������, ��������� � �������� ����� �� ��, ��� ������� � ���������� RacerColor
void ASpawnerButton::OnConstruction(const FTransform& FTransform)
{
	if (IsValid(Mesh))
	{
		Mesh->CreateDynamicMaterialInstance(0, Mesh->GetMaterial(0))->SetVectorParameterValue("Color", RacerColor);
		Mesh->CreateDynamicMaterialInstance(2, Mesh->GetMaterial(2))->SetScalarParameterValue("State", 0);
	}
	if (IsValid(StartPoint))
	{
		StartPoint->GetStaticMeshComponent()->CreateDynamicMaterialInstance(0, StartPoint->GetStaticMeshComponent()->GetMaterial(0))->SetVectorParameterValue("Color", RacerColor);
	}
	if (IsValid(EndPoint))
	{
		EndPoint->GetStaticMeshComponent()->CreateDynamicMaterialInstance(0, EndPoint->GetStaticMeshComponent()->GetMaterial(0))->SetVectorParameterValue("Color", RacerColor);
	}
}
//������ ������� ���������� ����� ���������
void ASpawnerButton::InteractionEvent_Implementation()
{
	//������ ���� ������ ��� ������� � ����� ����� ���������� ���� �������
	Mesh->CreateDynamicMaterialInstance(2, Mesh->GetMaterial(2))->SetScalarParameterValue("State", 1);
	FTimerHandle ActiveButtonTimer;
	GetWorld()->GetTimerManager().SetTimer(ActiveButtonTimer, [this]() {
		Mesh->CreateDynamicMaterialInstance(2, Mesh->GetMaterial(2))->SetScalarParameterValue("State", 0);
		}, 0.25f, 1);
	//���� �������� ��� ����������, ������� ���
	if (IsValid(RaceCharacterRef))
	{
		RaceCharacterRef->Destroy();
		return;
	}
	//���� ������� ��������� � �������� ����� � ��� ���� ��� ������������� �����, ������ ��� �� ��������� ����� � �������� � ���������� ��� BehaviorTree � ���� �� ��������� � �������� �����
	if (IsValid(StartPoint) && IsValid(EndPoint))
	{
		FActorSpawnParameters CharacterSpawnParameters;
		CharacterSpawnParameters.Owner = this;
		CharacterSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		FVector CharacterLocation = StartPoint->GetActorLocation();
		CharacterLocation.Z += 100;
		FRotator CharacterRotation = StartPoint->GetActorRotation();
		RaceCharacterRef = Cast<ACharacter>(GetWorld()->SpawnActor(RaceCharacterClass, &CharacterLocation, &CharacterRotation, CharacterSpawnParameters));
		if (IsValid(RaceCharacterRef))
		{
			UMaterialInstanceDynamic* DynMaterial = RaceCharacterRef->GetMesh()->CreateDynamicMaterialInstance(0, Mesh->GetMaterial(0));
			DynMaterial->SetVectorParameterValue("Color", RacerColor);

			AAIController* RaceCharacterController = Cast<AAIController>(RaceCharacterRef->GetController());
			if (IsValid(RaceCharacterController) && IsValid(BehaviorTree) && IsValid(StartPoint) && IsValid(StartPoint))
			{
				RaceCharacterController->RunBehaviorTree(BehaviorTree);
				RaceCharacterController->GetBlackboardComponent()->SetValueAsObject("StartPoint", StartPoint);
				RaceCharacterController->GetBlackboardComponent()->SetValueAsObject("EndPoint", EndPoint);
			}
		}
	}
}