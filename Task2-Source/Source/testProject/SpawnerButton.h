// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "InteractionInterface.h"
#include "Engine/StaticMeshActor.h"
#include "BehaviorTree/BehaviorTree.h"
#include "SpawnerButton.generated.h"

UCLASS()
class TESTPROJECT_API ASpawnerButton : public AActor, public IInteractionInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawnerButton();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName = "Start point")
		AStaticMeshActor* StartPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName = "End point")
		AStaticMeshActor* EndPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName = "Color")
		FLinearColor RacerColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName = "Character class")
		TSubclassOf<ACharacter> RaceCharacterClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName = "Character behavior tree")
		UBehaviorTree* BehaviorTree;

	virtual void InteractionEvent_Implementation() override;

private:

	UStaticMeshComponent* Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootMesh"));

	ACharacter* RaceCharacterRef;

	void OnConstruction(const FTransform& Transform) override;
};
