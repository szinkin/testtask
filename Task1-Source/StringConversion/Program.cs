﻿using System;
using System.Collections.Generic;

namespace StringConversion
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                string outputLine = "";
                Console.WriteLine("Write some text:");
                string inputLine = Console.ReadLine();
                //Вводим строку и чекаем пустая она или нет
                if (inputLine == "")
                {
                    return;
                }
                //Разбиваем строку на символы
                List<char> inputCharArr = new List<char>(inputLine);
                //В цикле находм повторения и выном их в отдельный лист
                for (int i = 0; i < inputCharArr.Count; i++)
                {
                    if (FindLetterInArray(inputCharArr[i], inputCharArr))
                    {
                        outputLine += ")";
                    }
                    else
                    {
                        outputLine += "(";
                    }
                }
                Console.WriteLine(inputLine + " => " + outputLine);
            }
        }

        //Функция для проверки повторяющихся символов
        static bool FindLetterInArray(char letter, List<char> charArray)
        {
            if (char.IsLetter(letter))
            {
                if ((charArray.FindAll(x => x == char.ToLower(letter)).Count + charArray.FindAll(x => x == char.ToUpper(letter)).Count) < 2)
                {
                    return false;
                }
            }
            else if (charArray.FindAll(x => x == letter).Count < 2)
            {
                return false;
            }
            return true;
        }
    }
}